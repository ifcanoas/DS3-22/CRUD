
package crud;

import java.net.URL;
import java.util.ResourceBundle;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Label;
import javafx.scene.control.RadioButton;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;
import model.Carro;
import model.Moto;
import model.Veiculo;

/**
 *
 * @author marci
 */
public class LocadoraVeiculoController implements Initializable {
    
    @FXML
    private Label label;
    
    
    @FXML
    private TextField placaInput;
    
    @FXML
    private TextField nomeInput;
    
    @FXML
    private TableColumn<Veiculo, ?> nome;

    @FXML
    private TableColumn<Veiculo, ?> placaColuna;

    @FXML
    private TableColumn<Veiculo, ?> tipoColuna;
    
    @FXML
    private TableView<Veiculo> tabela;
    
    @FXML
    private RadioButton radioMoto;


    
    private ObservableList<Veiculo> veiculos;
    
    private Veiculo veiculoEdicao = null;
    
    @Override
    public void initialize(URL url, ResourceBundle rb) {

        veiculos = tabela.getItems();//Vincula a lista da tabela a lista manipulavel

        //Atribui o elemento a célula (dica crie um objeto fake para coisas mais complexas)
        nome.setCellValueFactory(new PropertyValueFactory<>("nome"));
        placaColuna.setCellValueFactory(new PropertyValueFactory<>("placa"));
        tipoColuna.setCellValueFactory(new PropertyValueFactory<>("tipo"));

        
    }
    
    public void cadastrar() {
        Veiculo veiculoTemp ;
        
        if(radioMoto.isSelected()){
            veiculoTemp = new Moto();
        }else{
            veiculoTemp = new Carro();
        }      
        
        veiculoTemp.setNome(nomeInput.getText());
        veiculoTemp.setPlaca(Double.parseDouble(placaInput.getText()));//Vai cair na prova
        
        veiculos.add(veiculoTemp);        
               
        label.setText("Cadastrado com sucesso");
    }
    
    public void editar(){
        //Voce deveria testar se tem algo selecionado ^^
        
        veiculoEdicao = tabela.getSelectionModel().getSelectedItem();//pega o veiculo selecionado
        nomeInput.setText(veiculoEdicao.getNome());
        
        //Esconde outros botoes e exibe o fim
        
    }
    
    public void editarFim(){
        veiculoEdicao.setNome(nomeInput.getText());

        tabela.refresh();
    }
    
    public void deletar(){
        Veiculo v =  tabela.getSelectionModel().getSelectedItem();//pega o veiculo selecionado
       veiculos.remove(v);
       tabela.refresh();
    
    }
     
    
    
}
