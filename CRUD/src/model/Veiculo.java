/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package model;

/**
 *
 * @author marci
 */
public abstract class Veiculo {
    
    private String nome;
    private Double placa ;

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public Double getPlaca() {
        return  placa;
    }

    public void setPlaca(Double placa) {
        this.placa = placa;
    }
    
    public String getTipo(){
        return this.getClass().getSimpleName();
    }
    
}
